<?php
/**
 * @file
 * Contains \Drupal\collect\Form\RelationTypeDeleteForm.
 */

namespace Drupal\collect\Form;


use Drupal\Core\Entity\EntityDeleteForm;

/**
 * Delete confirmation form class for relation type entities.
 */
class RelationTypeDeleteForm extends EntityDeleteForm {

}
