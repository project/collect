<?php
/**
 * @file
 * Contains \Drupal\collect\Form\RelationDeleteForm.
 */

namespace Drupal\collect\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Delete confirm form for relations.
 */
class RelationDeleteForm extends ContentEntityDeleteForm {

}
