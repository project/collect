<?php
/**
 * @file
 * Contains \Drupal\collect\Relation\RelationPluginInterface.
 */

namespace Drupal\collect\Relation;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for relation plugins.
 */
interface RelationPluginInterface extends PluginInspectionInterface {

}
