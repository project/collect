<?php
/**
 * @file
 * Contains \Drupal\collect\Relation\RelationPluginBase.
 */

namespace Drupal\collect\Relation;

use Drupal\Core\Plugin\PluginBase;

/**
 * Base class for relation plugins.
 */
abstract class RelationPluginBase extends PluginBase implements RelationPluginInterface {

}
