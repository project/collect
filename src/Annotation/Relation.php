<?php
/**
 * @file
 * Contains \Drupal\collect\Annotation\Relation.
 */

namespace Drupal\collect\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for the Relation plugin type.
 *
 * @Annotation
 */
class Relation extends Plugin {

}
