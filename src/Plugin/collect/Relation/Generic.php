<?php
/**
 * @file
 * Contains \Drupal\collect\Plugin\collect\Relation\Generic.
 */

namespace Drupal\collect\Plugin\collect\Relation;

use Drupal\collect\Relation\RelationPluginBase;

/**
 * Generic, undefined relation.
 *
 * @Relation(
 *   id = "generic",
 *   label = @Translation("Generic")
 * )
 */
class Generic extends RelationPluginBase {

}
