<?php
/**
 * @file
 * Contains \Drupal\collect_client\Annotation\CollectClientItemHandler.
 */

namespace Drupal\collect_client\Annotation;
use Drupal\Component\Annotation\Plugin;

/**
 * Class CollectClientItemHandler
 *
 * @package Drupal\collect_client\Annotation
 * @Annotation
 */
class CollectClientItemHandler extends Plugin {

  protected $weight;
}
