<?php

/**
 * @file
 * Install file for Collect Demo.
 */

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\search_api\Entity\Index;
use Drupal\user\Entity\User;

/**
 * The state key for storing the demo user ID.
 */
const COLLECT_DEMO_STATE_USER = 'collect_demo.user';

/**
 * Implements hook_install().
 */
function collect_demo_install() {
  // Override default configuration.
  \Drupal::configFactory()->getEditable('collect.settings')->set('entity_capture', [
    'comment' => [
      'reference_fields' => ['uid'],
      'continuous' => ['default' => 'all', 'bundles' => []],
    ],
    'node' => [
      'reference_fields' => ['uid'],
      'continuous' => ['default' => 'all', 'bundles' => []],
    ],
    'taxonomy_term' => [
      'continuous' => ['default' => 'all', 'bundles' => []],
    ],
    'user' => [
      'continuous' => ['default' => 'all', 'bundles' => []],
    ],
  ])->save();

  // Override default host property in the model configuration and replace it
  // with current host.
  $schema_uri = 'http://schema.md-systems.ch/collect/0.0.1/collectjson/' . \Drupal::request()->getHost() . '/entity';
  \Drupal::configFactory()->getEditable('collect.model.collect_demo')->set('uri_pattern', "$schema_uri/node/collect_demo")->save();
  \Drupal::configFactory()->getEditable('collect.model.user')->set('uri_pattern', "$schema_uri/user")->save();
  \Drupal::service('router.builder')->rebuild();

  // Load/create a user with a sample email.
  if ($matched_users = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['mail' => 'collect.demo.user@example.com'])) {
    $user = reset($matched_users);
  }
  else {
    $user = User::create(['name' => 'Collect Demo User', 'mail' => 'collect.demo.user@example.com']);
    $user->save();
  }
  \Drupal::state()->set(COLLECT_DEMO_STATE_USER, $user->id());

  // Create a node that will be automatically captured.
  $node = Node::create([
    'title' => t('Collect Demo'),
    'type' => 'collect_demo',
    'uid' => $user->id(),
    'body' => [
      'value' => '<p>' . t('Welcome to the Collect Demo module!') . '</p>'
        . '<p>' . t('This demo module has added a bit of Collect-related content and configuration to your site—this introduction among other things. A few other modules are also enabled, in order to showcase some important features: <a href="@crm_core_url">CRM Core</a>, <a href="@search_api_url">Search API</a> and <a href="@inmail_url">Inmail</a> (Inmail Demo and Inmail Collect).', ['@crm_core_url' => 'http://github.com/md-systems/crm_core', '@search_api_url' => 'http://www.drupal.org/project/search_api', '@inmail_url' => 'https://www.drupal.org/project/inmail']) . '</p>'
        . '<p>' . t('Most of the Collect-related parts of the user interface are equipped with help texts to facilitate usage of Collect. These are a few hints to get you started:') . '</p>'
        . '<ul>'
        . '  <li>' . t('Check out the <a href="@containers_url">container list</a>. That\'s where you can overview all the collected data. There are some examples already.', ['@containers_url' => Url::fromUri('base:admin/content/collect')->toString()]) . '<ul>'
        . '    <li>' . t('This introduction node has been captured, along with the author user. You can <a href="@capture_url">capture any content entity</a>.', ['@capture_url' => Url::fromUri('base:admin/content/collect/capture')->toString()]) . '</li>'
        . '    <li>' . t('The web page at http://www.drupal.org. <a href="@fetch_url">Fetch more online content</a>.', ['@fetch_url' => Url::fromUri('base:admin/content/collect/url')->toString()]) . '</li>'
        . '  </ul></li>'
        . '  <li>' . t('Using the <a href="@inmail_paste_url">Inmail demo paste form</a> you can collect an email message.', ['@inmail_paste_url' => Url::fromUri('base:admin/config/system/inmail/paste')->toString()]) . '</li>'
        . '  <li>' . t('The <a href="@models_url">model overview</a> is where you configure how the various types of data are interpreted.', ['@models_url' => Url::fromUri('base:admin/structure/collect/model')->toString()]) . '</li>'
        . '  <li>' . t('A <strong>Search API index</strong> is set up. You can <a href="@index_url">inspect its configuration</a> or <a href="@search_url">view the search results</a> of the index.', ['@index_url' => Url::fromUri('base:admin/config/search/search-api/index/collect_demo')->toString(), '@search_url' => Url::fromUri('base:collect/demo-search')->toString()]) . '</li>'
        . '  <li>' . t('Some models are configured for <strong>processing</strong> new data. <a href="@contacts_url">Contact</a> and <a href="@activities_url">activity data</a> are reflected with the CRM Core framework.', ['@contacts_url' => Url::fromUri('base:crm-core/contact')->toString(), '@activities_url' => Url::fromUri('base:crm-core/activity')->toString()]) . '</li>'
        . '  <li>' . t('Optionally, you can install Collect Client to find out how data can be sent from different sites to be stored and processed centrally. Of course, real usage of Collect Client probably only makes sense on a different site than where the main Collect module is installed.') . '</li>'
        . '</ul>',
      'format' => 'basic_html',
    ],
  ]);
  $node->save();

  // Set the node as the front page.
  \Drupal::configFactory()->getEditable('system.site')->set('page.front', '/node/' . $node->id())->save();

  // Fetch a web page.
  collect_fetch_service()->fetch('http://www.drupal.org');

  // Reindex containers after new containers have been created.
  if ($index = Index::load('collect_demo')) {
    $index->disable()->save();
    $index->enable()->save();
  }
}

/**
 * Implements hook_uninstall().
 */
function collect_demo_uninstall() {
  $uid = \Drupal::state()->get(COLLECT_DEMO_STATE_USER);
  if ($user = User::load($uid)) {
    $user->delete();
  }
}
