<?php
/**
 * @file
 * Contains \Drupal\collect_test\Plugin\collect\Relation\Hugs.
 */

namespace Drupal\collect_test\Plugin\collect\Relation;

use Drupal\collect\Relation\RelationPluginBase;

/**
 * Test relation plugin.
 *
 * @Relation(
 *   id = "test_hugs",
 *   label = @Translation("Hugs")
 * )
 */
class Hugs extends RelationPluginBase {

}
